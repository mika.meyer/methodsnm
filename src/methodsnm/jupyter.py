import import_hack
from methodsnm.fe_1d import P1_Segment_FE
from methodsnm.visualize import DrawSegmentFE
p1 = P1_Segment_FE()
DrawSegmentFE(p1)

from methodsnm.fe_1d import P1Mod_Segment
p1m = P1Mod_Segment()
DrawSegmentFE(p1m)

