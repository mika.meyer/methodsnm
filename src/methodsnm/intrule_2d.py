"""
This module provides classes for numerical integration rules in 2D (triangles).
"""

from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.intrule import IntRule
import sympy as sy

class IntRuleTriangle(IntRule):
    """
    Abstract base class for numerical integration rules on triangle.
    """
    def __init__(self):
        pass

class EdgeMidPointRule(IntRuleTriangle):
    """
    Class for the midpoint rule for 1D numerical integration.
    """
    def __init__(self):
        """
        Initializes the midpoint rule with the given interval.

        """
        self.nodes = array([[0.5,0.0], [0.5,0.5], [0.0,0.5]])
        self.weights = array([1.0/6.0, 1.0/6.0, 1.0/6.0])
        self.exactness_degree = 1


class NewtonCotesRule_2d(IntRuleTriangle):
    def __init__(self, n):
        self.nodes = array([[0+i/n, 0+j/n] for i in range(n+1) for j in range(n-i+1)])
        x, y = sy.symbols('x, y')
        monomials = [x**i * y**j for i in range(n+1) for j in range(n-i+1)]
        integrated_monomials = np.empty(len(monomials))
        value_matrix = np.empty((len(monomials), len(self.nodes)))
        for i, monome in enumerate(monomials):
            integrated_monomials[i] = float(monome.integrate((x, 0, 1 - y)).integrate((y, 0, 1)).evalf())
            for j in range(len(self.nodes)):
                value_matrix[i][j] = (float(monome.subs([(x, self.nodes[j][0]), (y, self.nodes[j][1])]).evalf()))

        self.weights = np.linalg.lstsq(value_matrix, integrated_monomials, rcond=None)[0]

        self.exactness_degree = n

from methodsnm.intrule_1d import NP_GaussLegendreRule
from methodsnm.intrule_1d import SP_GaussJacobiRule
class DuffyBasedRule(IntRule):
    def __init__(self, order):
        gp_points = max(order,0)//2+1
        self.gauss = NP_GaussLegendreRule(gp_points)
        self.gjacobi = SP_GaussJacobiRule(gp_points,alpha=1,beta=0)
        self.nodes = array([[(1-eta[0])*xi[0], eta[0]] for xi in self.gauss.nodes for eta in self.gjacobi.nodes])
        self.weights = array([w1*w2 for w1 in self.gauss.weights for w2 in self.gjacobi.weights])
        self.exactness_degree = 2*gp_points-1
