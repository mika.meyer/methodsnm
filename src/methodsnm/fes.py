from abc import ABC, abstractmethod
import numpy as np
from numpy import array
from methodsnm.fe_1d import *
from methodsnm.fe_2d import *


class FESpace:
    """
    Abstract base class for finite element spaces.
    """
    ndof = None
    mesh = None

    def __init__(self, mesh):
        pass

    @abstractmethod
    def finite_element(self, elnr):
        raise Exception("Not implemented - Base class should not be used")

    @abstractmethod
    def element_dofs(self, elnr):
        raise Exception("Not implemented - Base class should not be used")


class P1_Segments_Space(FESpace):

    def __init__(self, mesh, periodic=False):
        self.order = 1
        if not periodic:
            self.ndof = len(mesh.points)
        else:
            self.ndof = len(mesh.points) - 1
        self.mesh = mesh
        self.periodic = periodic
        self.first_dof = 0
        if self.periodic:
            self.last_dof = len(mesh.points) - 2
        else:
            self.last_dof = len(mesh.points) - 1

    def finite_element(self, elnr):
        return P1_Segment_FE()

    def element_dofs(self, elnr):
        if elnr == len(self.mesh.elements()) - 1 and self.periodic:
            return [self.last_dof, self.first_dof]
        else:
            return self.mesh.edges[elnr]


class P0_Segments_Space(FESpace):
    def __init__(self, mesh):
        self.order = 0
        self.ndof = len(mesh.points) - 1
        self.mesh = mesh

    def finite_element(self, elnr):
        return P0_Segment_FE()

    def element_dofs(self, elnr):
        return [elnr]


class P2_Segments_Space(FESpace):
    def __init__(self, mesh):
        self.order = 2
        self.ndof = 2 * len(mesh.points) - 1
        self.mesh = mesh

    def finite_element(self, elnr):
        return P2_Segment_FE()

    def element_dofs(self, elnr):
        if elnr == 0:
            return [2 * elnr, 2 * elnr + 1, 2 * elnr + 2]
        else:
            return [2 * elnr - 1, 2 * elnr + 1, 2 * elnr + 2]


class Lagrange_Segments_Space(FESpace):
    def __init__(self, mesh, order):
        self.order = order
        self.ndof = order * len(mesh.points) - 1
        self.mesh = mesh

    def finite_element(self, elnr):
        return Lagrange_Segment_FE(order=self.order)

    def element_dofs(self, elnr):
        return [self.order * elnr + i for i in range(self.order + 1)]


class P1_Triangle_Space(FESpace):
    def __init__(self, mesh):
        self.mesh = mesh
        self.ndof = len(self.mesh.vertices)

    def finite_element(self, elnr):
        return P1_Triangle_FE()

    def element_dofs(self, elnr):
        return self.mesh.faces[elnr]


class P2_Triangle_Space(FESpace):
    def __init__(self, mesh):
        self.mesh = mesh
        self.ndof = len(self.mesh.vertices) + len(self.mesh.edges)

    def finite_element(self, elnr):
        return P2_Triangle_FE()

    def element_dofs(self, elnr):
        return np.concatenate((self.mesh.faces[elnr],
                               self.mesh.faces2edges[elnr] +
                               np.array([len(self.mesh.points) for _ in (self.mesh.faces2edges[elnr])])))


class P3_Triangle_Space(FESpace):

    def __init__(self, mesh):
        self.ndof = len(mesh.points) + 2 * len(mesh.edges) + len(mesh.faces)
        self.nv = len(mesh.points)
        self.ned = len(mesh.edges)
        self.nf = len(mesh.faces)
        self.mesh = mesh
        self.fe = P3_Triangle_FE()

    def finite_element(self, elnr):
        return self.fe

    def element_dofs(self, elnr):
        dofs = np.empty(10, dtype=int)
        vnums = self.mesh.elements()[elnr]
        dofs[0:3] = vnums
        j = 3
        enums = self.mesh.faces2edges[elnr]
        ref_verts_list = [[1, 2], [0, 2], [0, 1]]
        for ref_edge, edge in enumerate(enums):
            tverts = vnums[ref_verts_list[ref_edge]]
            if (tverts[0] < tverts[1]):
                dofs[j] = self.nv + 2 * edge + 0
                j += 1
                dofs[j] = self.nv + 2 * edge + 1
                j += 1
            else:
                dofs[j] = self.nv + 2 * edge + 1
                j += 1
                dofs[j] = self.nv + 2 * edge + 0
                j += 1
        dofs[9] = self.nv + 2 * self.ned + elnr
        return dofs


class P1disc_Segments_Space(FESpace):

    def __init__(self, mesh):
        self.ndof = 2 * len(mesh.edges)
        self.mesh = mesh
        self.fe = P1_Segment_FE()

    def finite_element(self, elnr):
        return self.fe

    def element_dofs(self, elnr):
        return [2 * elnr, 2 * elnr + 1]


class Pk_IntLeg_Segments_Space(FESpace):

    def __init__(self, mesh, order=1):
        self.nv = len(mesh.points)
        self.ne = len(mesh.edges)
        self.order = order
        self.ndof = self.nv + self.ne * (order - 1)
        self.mesh = mesh
        self.fe = IntegratedLegendre_Segment_FE(order=self.order)

    def finite_element(self, elnr):
        return self.fe

    def element_dofs(self, elnr):
        offset = self.nv + elnr * (self.order - 1)
        dofs = [self.mesh.edges[elnr][0]] + [self.mesh.edges[elnr][1]] + [offset + i for i in range(0, self.order - 1)]
        return dofs


class P1Edge_Triangle_Space(FESpace):

    def __init__(self, mesh):
        self.ndof = len(mesh.edges)
        self.mesh = mesh
        self.fe = P1Edge_Triangle_FE()

    def finite_element(self, elnr):
        return self.fe

    def element_dofs(self, elnr):
        edges = self.mesh.faces2edges[elnr]
        return edges
