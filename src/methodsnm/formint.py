from abc import ABC, abstractmethod
import numpy as np
from numpy import array, einsum
from methodsnm.intrule import *
from methodsnm.fe import *
from numpy.linalg import det, inv
from methodsnm.meshfct import ConstantFunction

class FormIntegral(ABC):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

class LinearFormIntegral(FormIntegral):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_vector(self, fe, trafo):
        raise NotImplementedError("Not implemented")

class SourceIntegral(LinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_vector(self, fe, trafo, intrule=None):
        if intrule == None:
            intrule = select_integration_rule(fe.order + fe.order, fe.eltype)
        shape = fe.evaluate(intrule.nodes)
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        n_quad = len(intrule.nodes)
        source_vector = np.zeros(fe.ndof)
        jacobian = trafo.jacobian(intrule.nodes)[0]
        determinant = np.linalg.det(jacobian)
        for i in range(fe.ndof):
            for k in range(n_quad):
                source_vector[i] += intrule.weights[k]*abs(determinant) * coeffs[k] * shape[k][i]
        return source_vector


class BilinearFormIntegral(FormIntegral):

    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def compute_element_matrix(self, fe, trafo):
        raise NotImplementedError("Not implemented")


class MassIntegral(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1)):
        self.coeff = coeff

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule=None):
        if intrule == None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception('Finite elements must have the same el. type')
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)
        shapes_test = fe_test.evaluate(intrule.nodes)
        shapes_trial = fe_trial.evaluate(intrule.nodes)
        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        n_quad = len(intrule.nodes)
        mass_matrix = np.zeros((fe_test.ndof, fe_trial.ndof))
        jacobian = trafo.jacobian(intrule.nodes)
        determinant = np.linalg.det(jacobian)
        for i in range(fe_test.ndof):
            for j in range(fe_trial.ndof):
                for k in range(n_quad):
                    mass_matrix[i][j] += intrule.weights[k] * abs(determinant[k]) * coeffs[k] * shapes_test[k][i] * shapes_trial[k][j]
        return mass_matrix



class LaplaceIntegral(BilinearFormIntegral):

    def __init__(self, coeff=ConstantFunction(1), alpha=1):
        self.coeff = coeff
        self.alpha = alpha

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule=None):
        if intrule == None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception('Finite elements must have the same el. type')
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)

        jacobian = trafo.jacobian(intrule.nodes)
        determinant = np.linalg.det(jacobian)
        shapes_test = np.array([np.linalg.inv(jacobian)[i].T@fe_test.evaluate(intrule.nodes[i], deriv=True) for i in range(len(intrule.nodes))])
        shapes_trial = np.array([np.linalg.inv(jacobian)[i].T@fe_trial.evaluate(intrule.nodes[i], deriv=True) for i in range(len(intrule.nodes))])

        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        n_quad = len(intrule.nodes)
        laplace_matrix = np.zeros((fe_test.ndof, fe_trial.ndof))
        for i in range(fe_test.ndof):
            for j in range(fe_trial.ndof):
                for k in range(n_quad):
                    laplace_matrix[i][j] += coeffs[k] * intrule.weights[k] * abs(determinant[i]) * \
                                            np.dot(shapes_test[k][:, i], shapes_trial[k][:, j])
        return laplace_matrix

class ConvectionIntegral(BilinearFormIntegral):
    def __init__(self, coeff=ConstantFunction(1), beta=array([1, 1])):
        self.coeff = coeff
        self.beta = beta

    def compute_element_matrix(self, fe_test, fe_trial, trafo, intrule=None):
        if intrule == None:
            if fe_test.eltype != fe_trial.eltype:
                raise Exception('Finite elements must have the same el. type')
            intrule = select_integration_rule(fe_test.order + fe_trial.order, fe_test.eltype)

        jacobian = trafo.jacobian(intrule.nodes)
        determinant = np.linalg.det(jacobian)
        shapes_test = fe_test.evaluate(intrule.nodes)
        shapes_trial = np.array([np.linalg.inv(jacobian)[i].T@fe_trial.evaluate(intrule.nodes[i], deriv=True) for i in range(len(intrule.nodes))])

        coeffs = self.coeff.evaluate(intrule.nodes, trafo)
        n_quad = len(intrule.nodes)
        convection_matrix = np.zeros((fe_test.ndof, fe_trial.ndof))
        for i in range(fe_test.ndof):
            for j in range(fe_trial.ndof):
                for k in range(n_quad):
                    convection_matrix[i][j] += coeffs[k] * intrule.weights[k] * abs(determinant[i]) * \
                                            np.dot(self.beta, shapes_trial[k][:, j]) * shapes_test[k][i]
        return convection_matrix


