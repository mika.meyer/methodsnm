from abc import ABC, abstractmethod
import numpy as np
from numpy import array, e
from methodsnm.trafo import *
from methodsnm.mesh import *
from methodsnm.mesh_1d import *
from methodsnm.meshfct import *


from methodsnm.intrule import *
from scipy import sparse

class Form(ABC):
    integrals = None
    fes = None
    def __init__(self):
        raise NotImplementedError("Not implemented")

    def add_integrator(self, integrator):
        self.integrals.append(integrator)

    def __iadd__(self, integrator):
        self.add_integrator(integrator)
        return self

class LinearForm(Form):

    vector = None
    def __init__(self, fes=None):
        self.fes = fes
        self.integrals = []
    
    def assemble(self):
        self.vector = np.zeros(self.fes.ndof)
        mesh = self.fes.mesh
        for elnr, verts in enumerate(mesh.elements()):
            trafo = mesh.trafo(elnr)
            fe = self.fes.finite_element(elnr)
            dofs = self.fes.element_dofs(elnr)
            for integral in self.integrals:
                self.vector[dofs] += integral.compute_element_vector(fe, trafo)


class BilinearForm(Form):

    matrix = None
    def __init__(self, fes=None, fes_test=None, fes_trial=None):
        if fes is None and (fes_test is None or fes_trial is None) or all([f is not None for f in [fes,fes_test,fes_trial]]):
            raise Exception("Invalid arguments, specify either `fes` or `fes_test` and `fes_trial`")
        if fes_test is None:
            fes_test = fes
            fes_trial = fes

        self.fes_trial = fes_trial
        self.fes_test = fes_test
        self.fes = fes_test
        self.integrals = []
    
    def assemble(self):
        self.matrix = sparse.lil_matrix((self.fes_test.ndof, self.fes_trial.ndof))
        mesh = self.fes.mesh
        for elnr, verts in enumerate(mesh.elements()):
            trafo = mesh.trafo(elnr)
            fe_test = self.fes_test.finite_element(elnr)
            dofs_test = self.fes_test.element_dofs(elnr)
            fe_trial = self.fes_trial.finite_element(elnr)
            dofs_trial = self.fes_trial.element_dofs(elnr)
            elmat = np.zeros((len(dofs_test), len(dofs_trial)))
            for integral in self.integrals:
                elmat += integral.compute_element_matrix(fe_test, fe_trial, trafo)
            for i, dofi in enumerate(dofs_test):
                for j, dofj in enumerate(dofs_trial):
                    self.matrix[dofi,dofj] += elmat[i,j]
        self.matrix = self.matrix.tocsr()

def error(u_h, u_exact):
    total_error = 0
    for elnr, verts in enumerate(u_h.mesh.elements()):
        fe = u_h.fes.finite_element(elnr)
        intrule = select_integration_rule(2 * fe.order, fe.eltype)
        trafo = u_h.mesh.trafo(elnr)
        u_h_values = u_h.evaluate(intrule.nodes, trafo)
        u_exact_values = u_exact.evaluate(intrule.nodes, trafo)
        difference_squared = np.array([(u_exact_values[i] - u_h_values[i])**2 for i in range(len(intrule.nodes))])
        weights = abs(np.linalg.det(trafo.jacobian(intrule.nodes)) * intrule.weights)
        integral = np.dot(weights, difference_squared)
        total_error += integral

    return total_error**(1/2)

def dirichlet(matrix, vector, fes):
    n = len(vector)
    if fes.finite_element(0).dim == 1:
        boundary_dofs = fes.mesh.bndry_vertices
    else:
        if fes.finite_element(0).dim == 2 and fes.finite_element(0).order == 1:
            boundary_dofs = array(fes.mesh.bndry_vertices)
        elif fes.finite_element(0).dim == 2 and fes.finite_element(0).order == 2:
            boundary_dofs = np.concatenate((array(fes.mesh.bndry_vertices),
                                            array(fes.mesh.bndry_edges) + array([len(fes.mesh.points)])))

        elif fes.finite_element(0).dim == 2 and fes.finite_element(0).order == 3:
            boundary_dofs = np.concatenate((array(fes.mesh.bndry_vertices), array([2])*array(fes.mesh.bndry_edges) + array([len(fes.mesh.points)]),
                                            array([2])*array(fes.mesh.bndry_edges) + array([len(fes.mesh.points) + 1])))
        else:
            boundary_dofs = []
    for i, row in enumerate(matrix):
        if i in boundary_dofs:
            matrix[i] = np.identity(n)[i]
            matrix[:, i] = np.identity(n)[i]
            vector[i] = 0
    return matrix, vector


