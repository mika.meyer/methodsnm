from import_hack import *
from methodsnm.mesh_2d import *
from methodsnm.visualize import *
mesh = StructuredRectangleMesh(10, 10)
# DrawMesh2D(mesh)
from methodsnm.fes import *
p1fes = P1_Triangle_Space(mesh)
from methodsnm.forms import *
from methodsnm.formint import *


blf = BilinearForm(p1fes)
c = GlobalFunction(lambda x: 1, mesh=mesh)
blf += LaplaceIntegral(c)
blf += MassIntegral(c)
blf.assemble()

lf = LinearForm(p1fes)
f = GlobalFunction(lambda x: x[0]*x[1], mesh=mesh)
lf += SourceIntegral(f)
lf.assemble()

uh = FEFunction(p1fes)
from scipy.sparse.linalg import spsolve
uh.vector = spsolve(blf.matrix, lf.vector)
DrawFunction2D(uh)
