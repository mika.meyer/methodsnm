from import_hack import *
from methodsnm.mesh_2d import *
from methodsnm.visualize import *
from matplotlib import pyplot as plt
from methodsnm.fes import *
from methodsnm.forms import *
from methodsnm.formint import *
from numpy import pi, cos, sin, e, array, tan, log
import numpy as np
import time
import pandas as pd

N_list = [2, 3, 4, 5, 6, 8, 10]
alphas_list = [1, 0.1, 0.01]
mappings_list = [lambda x, y: (x, y), lambda x, y: (x**0.5, y**0.5),
                 lambda x, y: (np.arctan(tan(1)*x), np.arctan(tan(1)*y))]
df_list = [pd.DataFrame(columns = ["l2_error", "Pe", "time"], index=[f'N={N}, alpha={alpha}' for N in N_list for alpha in alphas_list])
           for _ in range(len(mappings_list) + 1)]

fig, axs = plt.subplots(len(alphas_list), len(mappings_list) + 1, sharey="row")


def error_study(N, alpha, mapping_index=0, plot_functions=False):
    start_time = time.time()

    h = 1/N

    beta = array([1, 1])
    gamma = 0

    if mapping_index <= len(mappings_list) - 1:
        mesh = StructuredRectangleMesh(N, N, mapping=mappings_list[mapping_index])
    else:
        def a(alpha):
            if alpha>=1:
                return 1
            else:
                return (alpha * (e ** (-(alpha-1))))**0.5
        #mesh = StructuredRectangleMesh(N, N, mapping=lambda x, y: (np.arctan(tan(1)*x)**a(alpha), np.arctan(tan(1)*y)**a(alpha)))
        mesh = StructuredRectangleMesh(N, N, mapping=lambda x, y: (x**a(alpha), y**a(alpha)))
    #DrawMesh2D(mesh)
    fes = P3_Triangle_Space(mesh)

    pe = 2*max([abs(beta[i]) for i in range(len(beta))])/(h*alpha)

    blf = BilinearForm(fes)
    c = GlobalFunction(lambda x: alpha, mesh=mesh)
    blf += LaplaceIntegral(c)
    blf += ConvectionIntegral(beta=beta)
    blf.assemble()

    lf = LinearForm(fes)
    conv_diff = lambda x, alpha, beta, gamma: \
        beta[0]*(x[1] + (e**(beta[1]*x[1]/alpha) - 1)/
                 (1 - e**(beta[1]/alpha))) + \
        beta[1]*(x[0] + (e**(beta[0]*x[0]/alpha) - 1) /
                 (1 - e**(beta[0]/alpha)))
    f = GlobalFunction(lambda x: conv_diff(x, alpha, beta, gamma), mesh = mesh)

    lf += SourceIntegral(f)
    lf.assemble()


    uh = FEFunction(fes)
    from scipy.sparse.linalg import spsolve
    uh.vector = spsolve(*dirichlet(blf.matrix, lf.vector, fes))

    u_exact_conv_diff = lambda x, alpha, beta, gamma: \
        (x[0] + (e**(beta[0]*x[0]/alpha) - 1)/
                 (1 - e**(beta[0]/alpha))) * \
        (x[1] + (e**(beta[1]*x[1]/alpha) - 1) /
                 (1 - e**(beta[1]/alpha)))

    u_exact = GlobalFunction(lambda x: u_exact_conv_diff(x, alpha=alpha, beta=beta, gamma=gamma), mesh=mesh)

    end_time = time.time()
    elapsed_time = end_time - start_time
    if plot_functions:
        DrawFunction2D(u_exact)
        DrawFunction2D(u_exact, contour=True)


        DrawFunction2D(uh)
        DrawFunction2D(uh, contour=True)

    l2diff = error(uh, u_exact)
    global df_list
    df_list[mapping_index].loc[f'N={N}, alpha={alpha}', :] = [l2diff, pe, elapsed_time]


for N in N_list:
   for alpha in alphas_list:
       for i in range(len(mappings_list) + 1):
           error_study(N, alpha, mapping_index=i)

for i, df in enumerate(df_list):
   print(df)
   for j in range(len(alphas_list)):
       axs[j][i].set_yscale("log")
       axs[j][i].plot(N_list, df["l2_error"][j::len(alphas_list)])
       axs[j][i].set_title(f"a: {alphas_list[j]}, m: {i}")

fig.suptitle('L2 Error for different mappings and alphas')
plt.tight_layout()
plt.show()

#error_study(5, 0.1, 3, plot_functions=True)
