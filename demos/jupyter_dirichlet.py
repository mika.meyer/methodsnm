from import_hack import *
from methodsnm.mesh_2d import *
from methodsnm.visualize import *
N = 5
mesh = StructuredRectangleMesh(N,N)
h = 1.0/N
DrawMesh2D(mesh)
from methodsnm.fes import *
fes = P3_Triangle_Space(mesh)
from methodsnm.forms import *
from methodsnm.formint import *
from numpy import pi, cos, sin

blf = BilinearForm(fes)
c = GlobalFunction(lambda x: 1, mesh = mesh)
blf += LaplaceIntegral(c)
blf.assemble()

lf = LinearForm(fes)
f = GlobalFunction(lambda x: 2 * pi**2 * sin(pi*x[0]) * sin(pi*x[1]), mesh = mesh)
lf += SourceIntegral(f)
lf.assemble()
plt.spy(blf.matrix, markersize=4*h)

A, b = dirichlet(blf.matrix, lf.vector, fes)
print(blf.matrix.shape, " -> ", A.shape)
uh = FEFunction(fes)
from scipy.sparse.linalg import spsolve

uh.vector = spsolve(A, b)
DrawFunction2D(uh, contour=False)
DrawFunction2D(uh, contour=True)
uex = GlobalFunction(lambda x: sin(pi*x[0]) * sin(pi*x[1]), mesh = mesh)
l2diff = error(uh, uex)
print("l2diff =", l2diff)
