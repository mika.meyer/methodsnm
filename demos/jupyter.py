import numpy as np

import import_hack
from methodsnm.fe_1d import P1_Segment_FE
from methodsnm.visualize import DrawSegmentFE
p1 = P1_Segment_FE()
#DrawSegmentFE(p1)

from methodsnm.fe_1d import P1Mod_Segment
p1m = P1Mod_Segment()
#DrawSegmentFE(p1m)

from methodsnm.fe_1d import Lagrange_Segment_FE
lag = Lagrange_Segment_FE(order=5)
#print(lag)
#DrawSegmentFE(lag)

from methodsnm.fe_1d import Legendre_Segment_FE
from methodsnm.recpol import RecursivePolynomial, LegendreSegment


recpol = LegendreSegment()
leg = Legendre_Segment_FE(order=5)
#print(leg)
#DrawSegmentFE(leg)

from methodsnm.fe_1d import Jacobi_Segment_FE
jac = Jacobi_Segment_FE(order=5, alpha=0.5, beta=0)
#print(jac)
#DrawSegmentFE(jac)

from methodsnm.fe_1d import IntegratedLegendre_Segment_FE
ilg = IntegratedLegendre_Segment_FE(order=5)
print(ilg)
DrawSegmentFE(ilg)

from methodsnm.fe_2d import P2_Triangle_FE
from methodsnm.visualize import DrawTriangleFE
#p2 = P2_Triangle_FE()
#DrawTriangleFE(p2,sampling=20)

from methodsnm.fe_2d import P3_Triangle_FE
#p3 = P3_Triangle_FE()
#DrawTriangleFE(p3,sampling=20)

from methodsnm.fe_2d import P1Edge_Triangle_FE
p1e = P1Edge_Triangle_FE()
DrawTriangleFE(p1e,sampling=20)
