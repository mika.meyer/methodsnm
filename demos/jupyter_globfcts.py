from import_hack import *
from methodsnm.mesh import *
from methodsnm.visualize import DrawMesh1D
from methodsnm.mesh_1d import *
from methodsnm.fes import *

m = Mesh1D([0,1],10)
fes = P1_Segments_Space(m, periodic=True)
for elnr,el in enumerate(m.elements()):
    print(el)
    print(fes.element_dofs(elnr))

from methodsnm.meshfct import *
from methodsnm.visualize import DrawFunction1D, DrawShapes

m = Mesh1D([0,1],3)
#DrawShapes(P1_Segments_Space(m, periodic=True))

#DrawShapes(P0_Segments_Space(m))

#DrawShapes(P2_Segments_Space(m))

#DrawShapes(Lagrange_Segments_Space(m, order=5), sampling=50)

from import_hack import *
from methodsnm.mesh_2d import *
from methodsnm.visualize import DrawMesh2D

#m = StructuredRectangleMesh(10, 10)
#DrawMesh2D(m)
from numpy import sin, cos, pi
#m = StructuredRectangleMesh(12, 3, mapping= lambda x,y: (cos(pi*x)*(1+y), sin(pi*x)*(1+y)))
#DrawMesh2D(m)
from methodsnm.visualize import DrawFunction2D
from methodsnm.meshfct import GlobalFunction
from numpy import sin, cos, pi
u = GlobalFunction(function=lambda x: sin(2*(x[0]+x[1])*pi), mesh=m)
#DrawFunction2D(u, sampling=5)

from methodsnm.fes import *
from methodsnm.meshfct import FEFunction
m = StructuredRectangleMesh(2, 2)
fes = P1_Triangle_Space(m)
uh = FEFunction(fes)
for i in range(fes.ndof):
    uh.vector[:] = 0
    uh.vector[i] = 1
#    DrawFunction2D(uh,shrink_eps=0.05)

from methodsnm.fes import P2_Triangle_Space
from methodsnm.meshfct import FEFunction
m = StructuredRectangleMesh(2, 2)
fes = P2_Triangle_Space(m)
uh = FEFunction(fes)
for i in range(fes.ndof):
    uh.vector[:] = 0
    uh.vector[i] = 1
#   DrawFunction2D(uh,shrink_eps=0.05)


from methodsnm.fes import P3_Triangle_Space
from methodsnm.meshfct import FEFunction
m = StructuredRectangleMesh(2, 2)
fes = P3_Triangle_Space(m)
uh = FEFunction(fes)
for i in range(fes.ndof):
    uh.vector[:] = 0
    uh.vector[i] = 1
    DrawFunction2D(uh,shrink_eps=0.05)
