import import_hack
from methodsnm.mesh_1d import *
from methodsnm.visualize import *
from methodsnm.fes import *
from methodsnm.forms import *

ne = 4
mesh = Mesh1D((0,1),ne)
fes = P1_Segments_Space(mesh)
#fes = Pk_IntLeg_Segments_Space(mesh,3)
#DrawShapes(fes)
from methodsnm.forms import *
from methodsnm.formint import *

try: #include solution module (if exists)
    from methodsnm.solution import *
except:
    pass

blf = BilinearForm(fes)
c = GlobalFunction(lambda x: 1, mesh = mesh)
blf += LaplaceIntegral(c)
blf += MassIntegral(c)
blf.assemble()

lf = LinearForm(fes)
f = GlobalFunction(lambda x: np.pi**2 * np.sin(np.pi * x) + np.sin(np.pi*x), mesh = mesh)
lf += SourceIntegral(f)
lf.assemble()
# print(blf.matrix, "\n", lf.vector)
uh = FEFunction(fes)
from scipy.sparse.linalg import spsolve
uh.vector = spsolve(*dirichlet(blf.matrix, lf.vector, fes))

u_exact = GlobalFunction(lambda x: np.sin(np.pi * x), mesh=mesh)

DrawFunction1D(uh)
DrawFunction1D(u_exact)
print(error(uh, u_exact))

h = 10
errors = []
convergence = []
for i in range(2, h):
    mesh = Mesh1D((0, 1), i)
    fes = P2_Segments_Space(mesh)
    blf = BilinearForm(fes)
    c = GlobalFunction(lambda x: 1, mesh=mesh)
    blf += LaplaceIntegral(c)
    blf += MassIntegral(c)
    blf.assemble()
    lf = LinearForm(fes)
    f = GlobalFunction(lambda x: np.pi**2 * np.cos(np.pi * x) + np.cos(np.pi*x), mesh=mesh)
    lf += SourceIntegral(f)
    lf.assemble()
    uh = FEFunction(fes)
    uh.vector = spsolve(blf.matrix, lf.vector)
    u_exact = GlobalFunction(lambda x: np.cos(np.pi * x), mesh=mesh)
    new_error = error(uh, u_exact)
    errors.append(new_error)
    if i >= 3:
        convergence.append(errors[-1]/errors[-2])

#print(errors)
#print(convergence)
