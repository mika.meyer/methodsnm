{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Finite elements"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Background / context\n",
    "* In the finite element method the domain of interest $\\Omega$ is *triangulated*, i.e. decomposed into simple *elements*\n",
    "* The purpose of this decomposition is:\n",
    "    1. to find *local* function spaces \n",
    "    2. and glue them together afterwards to meet regularity requirements (typically: continuity)\n",
    "* In this unit we will concentrate on the first step: *local* function spaces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"../graphics/mesh3D_one_element.png\" height=160 /> <img src=\"../graphics/mesh2D_one_element.png\" height=160 />"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The local-to-global connection is later on formed through the association of *degrees of freedom* (dofs) to the geometrical entities of the mesh, e.g. the vertices, edges, faces, cells, etc..\n",
    "\n",
    "Example idea:\n",
    "* put a dof to each vertex: you have as many dofs as vertices **globally**\n",
    "* **locally** you have as many dofs as vertices in the element\n",
    "* every dof only appears in as many elements as elements are connected to the vertex\n",
    "\n",
    "This however requires that the basis functions associated to the dofs are *local* in the sense that they vanish on elements that do not contain the dof."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Finite elements\n",
    "We distinguish two characterizations of finite elements. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Finite elements in the classical sense (Ciarlet)\n",
    "Finite elements in the classical sense consist of a triple $(T,V_T,\\Psi^T)$ of:\n",
    " * a domain of definition (e.g. a triangle) $T$\n",
    " * a finite dimensional functions space $V_T: T \\to \\mathbb{R}$, $\\operatorname{dim}(V_T) = n_T$\n",
    " * a set of $n_T$ linearly independent functionals that describe the degrees of freedom $\\Psi^T = \\{\\Psi^T_i\\}_{i=1,..,n_T}$, $\\quad\\Psi^T_i: V_T \\to \\mathbb{R}, i =1,..,n_T$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Finite elements from an implementational point of view\n",
    "In practice the finite element is often defined through:\n",
    " * a domain of definition (e.g. a triangle) $T$\n",
    " * a set of basis functions $ \\{ \\phi^T_i \\}_{i=1,..,n_T} $\n",
    "\n",
    "Note that then obviously $V_T = \\operatorname{span}(\\{\\phi^T_i\\})$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Often, both characterizations are known and match. \n",
    "Otherwise the basis functions can be constructed as the *dual* to the functionals, so that $\\Psi^T_i(\\phi_j) = \\delta_{ij}$ \n",
    "\n",
    "Note: $\\Psi^T_i(\\phi_j) = \\delta_{ij}$ is nice, but is not necessary!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Examples (1D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: `P1(Segment)`\n",
    "FE triple $(T,V_T,\\Psi^T)$:\n",
    "* domain: line segment $T = [0,1]$ (\"unit interval\")\n",
    "* Linear functions on the  $[0,1]$: $V_T = \\mathcal{P}^1(T) = \\{f(x) = a + bx\\}$\n",
    "* `dofs` associated with the vertices $0$ and $1$: $\\Psi^T_0(v) = v(0)$, $\\Psi^T_1(v) = v(1)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dual basis:\n",
    "* $\\phi_0 = 1-x$, $\\qquad$ ($\\phi_0(0)=1$, $\\phi_0(1)=0$)\n",
    "* $\\phi_1 = x$, $\\qquad$ ($\\phi_1(0)=0$, $\\phi_1(1)=1$)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import import_hack\n",
    "from methodsnm.fe_1d import P1_Segment_FE\n",
    "from methodsnm.visualize import DrawSegmentFE\n",
    "p1 = P1_Segment_FE()\n",
    "DrawSegmentFE(p1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-> [Implementation](../src/fe.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: `P1(Segment)` (different `dofs`/functionals)\n",
    "\n",
    "* domain: line segment $T = [0,1]$ (\"unit interval\")\n",
    "* Linear functions on the  $[0,1]$: $V_T = \\mathcal{P}^1(T) = \\{f(x) = a + bx\\}$\n",
    "* `dofs`: $\\Psi^T_0(v) = \\int_0^1 v(x) dx$, $\\Psi^T_1(v) = \\int_0^1 x v(x) dx$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\leadsto$ different dual basis:\n",
    "* $\\phi_0(x) = 3-2x$, $\\qquad$ ($\\Psi^T_0(\\phi_0)=1$, $\\Psi^T_0(\\phi_0)=0$)\n",
    "* $\\phi_1(x) = 6-6x$, $\\qquad$ ($\\Psi^T_0(\\phi_1)=0$, $\\Psi^T_0(\\phi_1)=1$)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: `Lagrange(Segment)` (Lagrange polynomials of order $k$)\n",
    "* domain: line segment $T = [0,1]$ (\"unit interval\")\n",
    "* polynomial functions on the  $[0,1]$: $V_T = \\mathcal{P}^k(T) = \\{ \\sum_{l=0}^k a_k x^k \\mid a \\in \\mathbb{R}^{k+1}\\}$\n",
    "* `dofs` associated to different nodes $x_j \\in [0,1]$: $\\Psi^T_j(v) = v(x_j)$, $j=0,..,k$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Why different sets of `dofs` / functionals?\n",
    "\n",
    "* local $\\leftrightarrow$ global: Functionals (`dofs`) may be shared\n",
    "    * Often `dofs` are associated to geometrical entities (e.g. vertices in 1D)\n",
    "    * Then $\\Psi^T_{p,i}(\\phi^T_{q,j}) = 0$ for $p\\neq q$ where $p,q$ are different geometrical entities is necessary ($\\leadsto$ 2D)\n",
    "* if no global constraints are imposed, other properties may be benefitial, e.g. efficient/stable evaluation, orthogonality, .."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Tasks** $\\leadsto$ [fe.py](../src/methodsnm/fe.py), [fe_1d.py](../src/methodsnm/fe_1d.py), [visualize.py](../src/methodsnm/visualize.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `FE1D`-1.\n",
    "Implement a finite element (inherited from `FE`) for $V_T = \\mathcal{P}^2([0,1])$ with $\\Psi^T_0(v)=v(0)$, $\\Psi^T_1(v)=v(1)$ and $\\Psi^T_2(v)=\\int_0^1 v dx$.\n",
    "The corresponding basis does not need to be *dual*, but we ask for $\\Psi^T_0(\\phi_j)=\\delta_{0j}$, $\\Psi^T_1(\\phi_j)=\\delta_{1j}$\n",
    "* First, develop a corresponding basis on paper\n",
    "* Then, put the implementation of the new finite element (named e.g. `P2Mod_Segment`) in `fe_1d.py`\n",
    "* Draw the basis functions in this notebook, e.g. with the following code snippet"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_1d import P2_Segment_FE\n",
    "p2 = P2_Segment_FE()\n",
    "DrawSegmentFE(p2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `FE1D`-2. \n",
    "Complete the implementation of `Lagrange_Segment_FE` in `fe_1d.py` and Draw the corresponding basis functions for order $5$. Choose equidistant points for testing (default).\n",
    "* Come up with an evaluation formula that works for arbitrary orders $k$\n",
    "* Replace the function `evaluate` in the `Lagrange_Segment_FE` class\n",
    "\n",
    "Note that the default filling of the `nodes` values is already implemented."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_1d import Lagrange_Segment_FE\n",
    "lag = Lagrange_Segment_FE(order=5)\n",
    "print(lag)\n",
    "DrawSegmentFE(lag)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This task should fix the tests in [test_fe_1d.py](../tests/test_fe_1d.py)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `FE1D`-3.\n",
    "Implement a [Legendre FE basis](https://en.wikipedia.org/wiki/Legendre_polynomials) $\\{\\ell_i(x)\\}_{i=0,..,k}$ for arbitrary order $k$. Legendre polynomials form an orthogonal basis of $\\mathcal{P}^k([-1,1])$ w.r.t. to the $L^2([-1,1])$ inner product with $\\int_{-1}^1 \\ell_i(x) \\ell_j(x) ~ dx = \\delta_{ij} \\frac{2}{2i+1}$. A finite element can be defined through:\n",
    "* domain: line segment $T = [-1,1]$ (\"centered unit interval\")\n",
    "* polynomial functions on $[-1,1]$: $V_T = \\mathcal{P}^k(T) = \\{ \\sum_{l=0}^k a_k x^k \\mid a \\in \\mathbb{R}^{k+1}\\}$\n",
    "* `dofs` associated to different **modes** based on the Legendre basis itself: $\\Psi^T_j(v) = \\int_{-1}^1 v(x) \\ell_j(x) ~ dx ~\\cdot (j+\\frac12)$, $j=0,..,k$, s.t. $\\Psi^T_j(\\ell_i) = \\delta_{ij}$\n",
    "\n",
    "There holds the following important characterization that simplifies the efficient implementation of the Legendre polynomials:\n",
    "* $\\ell_0(x) = 1$\n",
    "* $\\ell_1(x) = x$\n",
    "* and the recurrence relation for $n > 1$:\n",
    "    $$\n",
    "    (n+1) \\ell_{n+1}(x) = (2 n + 1) x \\ell_{n}(x) - n \\ell_{n-1}(x)\n",
    "    $$\n",
    "\n",
    "An implementation based on the recursion is easy when based on `RecursivePolynomial`s in [recpol.py](../src/methodsnm/recpol.py).\n",
    "\n",
    "Note that when implementing the Legendre FE basis you need to apply an affine transformation between $[0,1]$ and $[-1,1]$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.recpol import RecursivePolynomial, LegendrePolynomials\n",
    "import numpy as np\n",
    "leg = LegendrePolynomials()\n",
    "leg.plot_all(np.linspace(-1,1,100), 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_1d import Legendre_Segment_FE\n",
    "leg = Legendre_Segment_FE(order=5)\n",
    "print(leg)\n",
    "DrawSegmentFE(leg)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "\n",
    "* What are major advantages and disadvantages of the Legendre basis compared to the Lagrange basis?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `FE1D`-4.\n",
    "\n",
    "The Jacobi polynomials are a generalization of the Legendre polynomials.\n",
    "We will see them appearing later on again. For now we only need the following properties:\n",
    "* They are defined w.r.t. two parameters $\\alpha$, $\\beta$\n",
    "* They can defined through \n",
    "    * the starting functions:\n",
    "        * $P_0^{(\\alpha,\\beta)}(x) = 1$\n",
    "        * $P_1^{(\\alpha,\\beta)}(x) = (\\alpha + 1) + (\\alpha + \\beta + 2) \\frac{x-1}{2}$\n",
    "    * and [recurrence relations](https://en.wikipedia.org/wiki/Jacobi_polynomials#Recurrence_relations) for $n > 1$.\n",
    "\n",
    "Based on `RecursivePolynomial`s in [recpol.py](../src/methodsnm/recpol.py), also implement a FiniteElement with the Jacobi basis. \n",
    "\n",
    "* Add a test that checks that for $\\alpha=\\beta=0$ the Jacobi basis reduces to the Legendre basis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_1d import Jacobi_Segment_FE\n",
    "jac = Jacobi_Segment_FE(order=5, alpha=1, beta=0)\n",
    "print(jac)\n",
    "DrawSegmentFE(jac)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `FE1D`-5.\n",
    "\n",
    "To combine some advantages of nodal (like the Lagrange) and modal (like the Legendre) finite elements, we can also use a combination of both:\n",
    "* take $\\phi_0$ and $\\phi_1$ as P1 basis functions corresponding to the vertices $0$ and $1$\n",
    "* take $\\phi_k$ for $k>1$ as $\\phi_k(x) = L_{k}(2x-1)$ with $L_{k}(z) = \\int_{-1}^z \\ell_{k-1}(x) dx$ where $\\ell_{k-1}$ is the Legendre basis function of order $k-1$ (so that $L_k$ and $\\phi_k$ are of order k).\n",
    "\n",
    "For the integrated Legendre basis functions $L_k$ there is a again a characterization as a recursive polynomial that can be used to implement this finite element (no explicit integration formula needed).\n",
    "\n",
    "Task: Implement this finite element and draw the basis functions for order $5$.\n",
    "\n",
    "There holds for the integrated Legendre basis functions $L_k$:\n",
    "* $L_0(x) = x$\n",
    "* $L_1(x) = \\frac12 (x^2 - 1)$\n",
    "* $L_k(x) = \\frac{2k-3}{k} x L_{k-1}(x) - \\frac{k-2}{k} L_{k-2}(x)$ for $k>1$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.recpol import IntegratedLegendrePolynomials\n",
    "import numpy as np\n",
    "leg = IntegratedLegendrePolynomials()\n",
    "leg.plot_all(np.linspace(-1,1,100), 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_1d import IntegratedLegendre_Segment_FE\n",
    "ilg = IntegratedLegendre_Segment_FE(order=5)\n",
    "print(ilg)\n",
    "DrawSegmentFE(ilg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "What can you say about $\\int_0^1 \\phi_k'(x) \\phi_l'(x) dx$ for the (mixed) integrated Legendre basis?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Examples (2D)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: `P1(Triangle)`\n",
    "<img src=\"../graphics/seq_P1.png\" height=160 />\n",
    "\n",
    "FE triple $(T,V_T,\\Psi^T)$:\n",
    "* domain: Triangle $T = \\hat T = \\operatorname{conv}((0,0),(1,0),(0,1))$ (\"unit triangle\")\n",
    "* Linear functions on the  $T = \\hat T$: $V_T = \\mathcal{P}^1(T) = \\{f(\\mathbf{x}) = a + b~x_1 + c~x_2\\}$, $\\mathbf{x} = (x_1,x_2)$\n",
    "* `dofs` associated with the vertices at $v_0=(0,0)$, $v_1=(1,0)$ and $v_2=(0,1)$: $\\Psi^T_i(w) = w(v_i)$, $i=0,1,2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#%matplotlib qt\n",
    "from methodsnm.fe_2d import P1_Triangle_FE\n",
    "from methodsnm.visualize import DrawTriangleFE\n",
    "p1 = P1_Triangle_FE()\n",
    "DrawTriangleFE(p1,figsize=(12,5))\n",
    "DrawTriangleFE(p1,contour=True,figsize=(12,4))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: `Lagrange_P2(Triangle)`\n",
    "<img src=\"../graphics/seq_P2.png\" height=160 />\n",
    "\n",
    "FE triple $(T,V_T,\\Psi^T)$:\n",
    "* domain: Triangle $T = \\hat T = \\operatorname{conv}((0,0),(1,0),(0,1))$ (\"unit triangle\")\n",
    "* Linear functions on the  $T = \\hat T$: $V_T = \\mathcal{P}^2(T) = \\{f(\\mathbf{x}) = \\displaystyle \\sum_{\\substack{i,j=0\\\\ i+j \\leq 2}} a_{ij} ~ x_1^i ~ x_2^j \\mid a_{ij} \\in \\mathbb{R} \\}$, $\\mathbf{x} = (x_1,x_2)$\n",
    "* `dofs` associated with the vertices and the edge midpoints, $v_0$, $v_1$, $v_2$, $e_0 = (0.5,0.5)$, $e_1 = (0,0.5)$ and $e_2 = (0.5,0)$: \n",
    "    * vertex dofs: $\\Psi^T_i(w) = w(v_i)$, $i=0,1,2$\n",
    "    * edge dofs: $\\Psi^T_i(w) = w(e_{i-3})$, $i=3,4,5$\n",
    "\n",
    "Note: we number the edges so that edge $i$ is opposite to vertex $i$, i.e. vertex $i$ and vertex $j$ defined edge $3-i-j$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ***Tasks*** $\\leadsto$ [fe.py](../src/methodsnm/fe.py), [fe_2d.py](../src/methodsnm/fe_2d.py), [visualize.py](../src/methodsnm/visualize.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `FE2D`-1\n",
    "\n",
    "Implement the `Lagrange_P2(Triangle)` finite element in `fe_2d.py` and draw the basis functions.\n",
    "* Come up with an evaluation formula for the six basis function.\n",
    "* You may want to make use of the [barycentric coordinates](https://en.wikipedia.org/wiki/Barycentric_coordinate_system) of the triangle $\\lambda_0(\\mathbf{x}) = 1 - x_1 - x_2$, $\\lambda_1(\\mathbf{x}) = x_1 $, $\\lambda_2(\\mathbf{x}) = x_2$. Where do the barycentric coordinates vanish?\n",
    "<img src=\"../graphics/barycoords.png\" height=320 />\n",
    "\n",
    "\n",
    "* Implement the `evaluate`-function in the `P2_Triangle_FE` class in `fe_2d.py`\n",
    "* Draw the basis functions in this notebook, e.g. with the following code snippet\n",
    "\n",
    "Optional: add a test in `tests/test_fe_2d.py` that checks for a reasonable implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_2d import P2_Triangle_FE\n",
    "p2 = P2_Triangle_FE()\n",
    "DrawTriangleFE(p2,sampling=20, figsize=(12,6))\n",
    "DrawTriangleFE(p2,sampling=20, contour=True, figsize=(12,6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `FE2D`-2\n",
    "\n",
    "We now go to `P3(Triangle)` finite elements. While at the moment it may appear only as a good repition of the concepts for P2 and P1, we will later see that there is an effect that appears only for P3 and higher order elements that is not yet visible for P2 and P1. So, for later reference we will need the P3 element.\n",
    "\n",
    "Implement the `Lagrange_P3(Triangle)` finite element in `fe_2d.py` and draw the basis functions.\n",
    "* Come up with an evaluation formula for the basis functions. How many are there?\n",
    "* Implement the `evaluate`-function in a new `P3_Triangle_FE` class in `fe_2d.py`\n",
    "* Draw the basis functions in this notebook, e.g. with the following code snippet\n",
    "\n",
    "Optional: add a test in `tests/test_fe_2d.py` that checks for a reasonable implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_2d import P3_Triangle_FE\n",
    "p3 = P3_Triangle_FE()\n",
    "DrawTriangleFE(p3,sampling=20, contour=False, figsize=(20,6))\n",
    "DrawTriangleFE(p3,sampling=20, contour=True, figsize=(20,6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `FE2D`-3\n",
    "\n",
    "Implement the following FE triple $(T,V_T,\\Psi^T)$ (with corresponding dual basis):\n",
    "* domain: Triangle $T = \\hat T = \\operatorname{conv}((0,0),(1,0),(0,1))$ (\"unit triangle\")\n",
    "* Linear functions on the  $T = \\hat T$: $V_T = \\mathcal{P}^1(T) = \\{f(\\mathbf{x}) = a + b~x_1 + c~x_2\\}$, $\\mathbf{x} = (x_1,x_2)$\n",
    "* `dofs` associated with edge integrals: $\\Psi^T_i(w) = \\frac{1}{|e_i|} \\int_{e_i} w(s) ds$, $i=0,1,2$ where $e_i$ are the edges of the triangle.\n",
    "\n",
    "Note: The functions are linear on each edge, hence the integral is exactly computed by the midpoint rule.\n",
    "\n",
    "Optional:  Add a test in `tests/test_fe_2d.py` that checks for a reasonable implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from methodsnm.fe_2d import P1Edge_Triangle_FE\n",
    "p1e = P1Edge_Triangle_FE()\n",
    "DrawTriangleFE(p1e,sampling=12, contour=False, figsize=(10,3))\n",
    "DrawTriangleFE(p1e,sampling=2, contour=True, figsize=(10,3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Note:\n",
    "We will come back to finite element (implementations) later again, e.g. to\n",
    "  * implement (arbitrary) high order elements (nodal / modal)\n",
    "  * fix some aspects of the P3 (and higher order) elements (problem will only be revealed later)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
