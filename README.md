# Methods for Numerical Mathematics ( Methoden zur Numerischen Mathematik )


## Badges & Links to Deployments
* [![Binderhub](https://img.shields.io/badge/Binderhub-Jupyterlab-orange)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fmika.meyer%2Fmethodsnm/HEAD)
* [![lite-badge](https://img.shields.io/badge/Jupyterlite-yellow)](https://mika.meyer.pages.gwdg.de/methodsnm/jl)
* [![static-html](https://img.shields.io/badge/static_HTMLs-white)](https://mika.meyer.pages.gwdg.de/methodsnm/static.html)
* [![Docker](https://img.shields.io/badge/Dockerhub-blue)](https://gitlab.gwdg.de/mika.meyer/methodsnm/container_registry)
* [![API-docu](https://img.shields.io/badge/PyDoc-purple)](https://mika.meyer.pages.gwdg.de/methodsnm/doc/methodsnm.html)


